try {Vanui} catch(e) {Vanui={}}

//---o:
Vanui.Splitter = function(splitter_elm, resize_elm, div_position) {

    if (div_position == 'left' || div_position == 'right') {
        splitter_elm.classList.add('vanui-splitter')
    } else {
        splitter_elm.classList.add('vanui-splitter-h')
    }
    
    // Events
    splitter_elm.addEventListener('mousedown', function(e) {
        if (event.buttons == 1) {
            e.preventDefault()
            
            // Add Splitter Background Overlay
            if (document.querySelectorAll('.vanui-splitter-background').length == 0) {
                let split_bkgd = document.createElement('div')
                split_bkgd.className = 'vanui-splitter-background'
                document.body.appendChild(split_bkgd)
            }

            window.addEventListener('mousemove', resize)
            window.addEventListener('mouseup', stopResize)
            window.addEventListener('mouseleave', stopResize)
        }
    })
    
    function resize(event) {
        if (div_position == 'left') {
            resize_elm.style['flex-basis'] = event.pageX - resize_elm.getBoundingClientRect().left + 'px'
            resize_elm.style['flex-grow'] = 0
        } else if (div_position == 'right') {
            resize_elm.style['flex-basis'] = resize_elm.getBoundingClientRect().right - event.pageX + 'px'
            resize_elm.style['flex-grow'] = 0
        } else if (div_position == 'top') {
            resize_elm.style['flex-basis'] = event.pageY - resize_elm.getBoundingClientRect().top + 'px'
            resize_elm.style['flex-grow'] = 0
        } else if (div_position == 'bottom') {
            resize_elm.style['flex-basis'] = resize_elm.getBoundingClientRect().bottom - event.pageY + 'px'
            resize_elm.style['flex-grow'] = 0
        }
    }

    function stopResize() {
        document.body.removeChild(document.querySelector('.vanui-splitter-background'))
        window.removeEventListener('mousemove', resize)
        window.removeEventListener('mouseup', stopResize)
        window.removeEventListener('mouseleave', stopResize)
    }
    
    return {
        elm:splitter_elm,
    }
    
}