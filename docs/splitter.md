# Vanui.Splitter
The splitter widget allows a user to resize the ui by dragging an edge of a pane.

## Demo
[Splitter Demo](https://lucidlylogicole.gitlab.io/vanui/demos/splitter.html) - A vertical and horizontal splitter

## API
The splitter is set to work with a flex container and will resize one of the divs using flex-basis while the other flexes.

**HTML**  
Create a container, and 2 divs for with a splitter in the middle.

    <div id="container" style="display:flex">
        <div id="left_window" style="flex-basis:200px">
        <div id="splitter"></div>
        <div id="main_window" style="flex:1">
    </div>

**JS**

    Vanui.Splitter(document.getElementById('splitter'),
        document.getElementById('left_window'), 'left')

- **Vanui.Splitter(splitter_element, resize_element, position)**
    - `splitter_element` - the element that will be the draggable part of the splitter
    - `resize_element` - the div or element to resize
    - `position` - the position of the resizeable element compared to the flex element
        - available positions: `left,right,top,bottom`