# Vanui.Table
A keyboard friendly table widget with optional editing, sorting, and pagination.

## Demos
- [General Table](https://lucidlylogicole.gitlab.io/vanui/demos/table.html)
- [Table with Pagination](https://lucidlylogicole.gitlab.io/vanui/demos/table_pagination.html)

## Features
- optionally editable
- multiple column types (text,number,select,date,custom)
- custom columns with edit function specified
- single column sort (optional)
- pagination (optional)
- advanced keyboard cursor movement and editing
    - a lot of effort has been spent for an intuitive and nice keyboard editing experience, although slightly different than typical spreadsheets
- fixed/sticky row and column headers

----
## Quick Start Examples

### Create a Simple Table

**HTML** - create a container for the table

    <div id="table_container"></div>

**JS**

    let tbl = Vanui.Table({
        parent:'table_container',
        columns:['Column 1','Column B','Column 3rd'],
        data:[[1,2,3],[4,5,6]],
    })

### Create a Table using more options
**HTML** - create a container for the table

    <div id="table_container"></div>

**JS**

    let tbl = Vanui.Table({
        parent:'table_container',
        tableId:'newtable', // give an optional id for styling or reference
        columns:[
            {title:'Var X'},
            {title:'Twice X',type:'number'},
            {title:'X Squared',editable:0},
            {title:'Select',type:'select',items:['Option 1','Option B']},
            {title:'Date',type:'date'},
            {title:'Custom',type:'custom',editCell:function(rid,cid,val){}},
        ],
        data:[[1,2,3,'','',''],[4,5,6,'Option 1','2021-02-12','']],
        editable:1,   // table is editable
        sort:1,       // enable sorting
        pages:{enabled:1,size:20}  // use pagination
    })

----
## API
The vanui-table uses an index for columns and rows which may not correspond to the displayed row number or column number.

### **Table(options)**
- `options` - (object) all options are optional
    - `parent` - (string) - the id of the container element to add the table to
    - `tableId` - (string) - specify the element id to add when creating the table
    - `columns` - (array) - array of the columns
        - the items of the columns array can be a dictionary or a string
        - _column dict_ - if the column item is a dict, the following keys are available
            - `title` - (string) - the display text for the column
            - `type` - (string) - the type of column ('text', 'number', 'date', 'select', 'custom') the default is 'text'
            - `editable` - (boolean) - if the column is editable (default is true if the table is editable)
            - `editCell` - (function) if the `type=='custom'`, then you can specify the editCell function
            - `items` - (array) - if the `type=='select'`, then use items to specify the options for the combobox.
    - `data` - the data for the table.  multiple data formats are available:
        - tab delimited string
        - 2D array
        - for additional options use the `setData` function instead
    - `doc` - (dict) - if using the internal 'doc' structure (defined below), this can be provided instead of data
    - `editable` - (boolean) - is the table editable
    - `sort` - (boolean) - is sorting enabled
    - `pages` - (dict) - pagination options. by default, pagination is not enabled
        - `enabled` - (boolean) - enable pagination
        - `size` - (int) - row size for the pages
- **.element** - returns the created element containing the table


### Row Functions
- **.insertRow(options)** - insert a row
    - `options.before_rid` at optionally specified `rid`
    - `options.data` - optionally specify data to insert either as an array or an object of column ids to value `{cid:val}``
- **.deleteRow(rid)** - delete the row with row index `rid`
- **.rowCount()** - the number of rows

### Column Functions
- **addColumn(options)** (string or dict) add a column
    - `options` - if a string then that will be the title, otherwise the same column dictionary options can be utilized
        - `title` - (string) - the display text for the column
        - `type` - (string) - the type of column ('text', 'number', 'date', 'select', 'custom') the default is 'text'
        - `editable` - (boolean) - if the column is editable (default is true if the table is editable)
        - `editCell` - (function) if the `type=='custom'`, then you can specify the editCell function
        - `items` - (array) - if the `type=='select'`, then use items to specify the options for the combobox.
- **.deleteColumn(cid)** - delete the column and data at the column index
- **.columnCount()** - the number of columns
- **.setColumnVisible(cid, visible)** - show or hide the column
    - `cid` (int) - the column index
    - `visible` (boolean) - set the column visible if true or hide if false
- **.sortColumn(cid, descending, sort_function)** - sort the column
    - `cid` (int) - the column index
    - `descending` (boolean) - optionally specify whether to sort descending. leaving this as undefined will toggle the sort
    - `sort_function` - optionally provide a function to sort by

### Cell Functions
- **.cellVal(rid,cid)** - get the value of the cell
- **.cellElement(rid,cid)** - get the html td element
- **.selectCell(rid,cid)** - select the cell
- **.getCellIds(row,col)** - get the internal row and column ids (rid,cid) by specifying the current view row and column index (starting at 0)

### View / Pagination Functions
If pagination is enabled, the following functions are available
- **.nextPage()** - goto the next page
- **.prevPage()** - goto the previous page
- **.pageCount()** - the number of pages
- **.gotoPage(page)** - goto a specific page (starting at 1)
- **.clearView(clear_header)** - clear the table and optionally the header
- **.loadView()** - reload the view with the internal data. this function is not really needed and is called by setData, setDoc, etc.

### Data Functions
- **.setData(data,delim)** - set the table data (clears existing data)
    - `data` - can be a string or 2D array.
    - `delim` - optionally use if passing data as a string. The default delimiter is a tab `\t`
- **.getData()** - get the table data in a simple format
    - `{columns:['col 1','col b'], data:[[1,2],[3,4]]}`
- **.getText(delim)** - get the table data as a tab delimited string
    - `delim` - optionally specify the column delimiter (default is `\t`)

### Doc Functions
Vanui.Table has an internal data structure referenced here as a `doc`. This can be stored and loaded instead of the other data formats.
- **.getDoc(get_json)** - get the internal document
    - `get_json` (boolean) - get the format as json (default is false)
- **.setDoc(doc)** - set the internal document and load the view
    - `doc` (string or object) - can specify a json string or an object

### Events
- **cellChanged** - event dispatched when a cell value is changed
    - details = `{rid:rid,cid:cid,val:val}`
- **cellSelected** - event dispatched when a cell is selected
    - details = `{rid:rid,cid:cid}`
- **cellDblClick** - event dispatched when a cell is double clicked on
    - details = `{rid:rid,cid:cid}`

### Undocumented Functions
- Additional functions not listed here are considered undocumented
- Undocumented functions could change in the future, while the documented API will attempt to be stable.

----
## Keyboard Shortcuts

### Navigation
- `Arrow Up` - move cell up
- `Arrow Down` - move cell down
- `Arrow Left` - move cell left
- `Arrow Right` - move cell right
- `Home` - move to first row
- `End` - move to last row

### Editing
- `Key` - a character key will start editing the cell
- `Enter` - start editing cell or finish editing cell
- `Ctrl + C` - copy cell
- `Ctrl + V` - paste cell
- `Ctrl + X` - cut cell
- `Alt + Arrow Down` - while editing a select/combobox or date field, this will
show the drop down
- `Delete` - delete the cell
- `Ctrl + Delete` or `Alt + Delete` - delete the row
- `Escape` - stop editing and don't store changes in the cell

----
## Future Features
The following features are planned to be added next
- [ ] functions to show/hide rows
