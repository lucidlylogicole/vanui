# Vanui.Form
Build a form using a JavaScript config and get/set values.

## Demos
- [Form Demo](https://lucidlylogicole.gitlab.io/vanui/demos/form.html)

## Quick Start Example

    <div id="container"></div>

    <script>
        let fields= [
            {title:'Text Field',type:'text',tooltip:'a text field'},
            {title:'Number Field',type:'number'},
            {title:'Date Field',type:'date'},
            {title:'Combobox Field',type:'combobox',items:['Apple','Grape','Pizza']},
            {title:'Checkbox Field',type:'checkbox'},
        ]
        
        let form = Vanui.Form({parent:'container',fields:fields})
        form.getValues() // 
    </script>

## Documentation

### Vanui.Form

- **Form**`(options)` - the main form object
    - `options` (dict)
        - `parent` - (string) - id of element to add form fields to
        - `fields` - (array) - array of field objects
        - `wide` - (boolean) - set all fields to wide mode where the label is above the form field (default is 0 which is not wide)
        - `format` (int) - the default format used for the  getValues function (default is 0 or undefined)
    - **.element** - returns the element created containing the form
    - **.fields** - (array) list of all the field objects
    - **.addField(fieldD)** - add a field to the form
    - **.clearValues()** - clear all the field values (if defaults are specified, it will set to default)
    - **.setValue(field_index, val)** - set the value of the field at the field index
    - **.setValues(valueD)** - set a bunch of values
        - `valueD` - (dict) - a dictionary of ids to value or field index to value
    - **.getValue(field_index)** - get the value at the field index
    - **.getValues(format)** - get all field values (returns an array or dict)
        - `format` (int) - the return format.
            - `0` (default) an array
            - `1` a dictionary of the id:value

Example Use

    let fields = [......]
    let form = Vanui.Form({element:'container',fields:fields})
    form.getValues()

### Fields
The field object defines all the field information and settings. Depending on the field type, there may be additional options


**field options** - all fields have the following options and most are optional

- `type` - type of field
- `title` - the label text
- `id` - element id to use
- `tooltip` - hover over tooltip
- `attr` - (dict) - set additional html attributes
- `events` - (dict) - set event listeners (key=event name, value = event function)
- `val` - set the value for the field
- `default` - the default value for the field (will be set if val is not defined or when the form is cleared)

**Field Types**

- **button** - add a button
    - `click` - specify the click function
- **checkbox** - checkbox input
- **combobox** - a select field
    - `items` - (array) an array of the select options
        - the item in the array can be a single value or a dictionary specifying the title and value in the format:
            - `{title:'', val:''}`
- **date** - a date field
- **list** - a select field with options to make it a list
    - `items` - (array) an array of the select options
        - the item in the array can be a single value or a dictionary specifying the title and value in the format:
            - `{title:'', val:''}`
    - `size` - (int) - number of rows for the list
    - `multiple` - (boolean) set to true to allow multiple options to be selected
- **number** - a number field
    - `attr` (dict) - to set additional options like the min,max, step, use the attr definition 
        - ex: `{attr:{min:0,max:100}`
- **select** - same as the combobox or list depending on the options
- **separator** - add a separator. this does count as a field in the index, although the value will be undefined
    - `line` - (boolean) display a line for the separator (default is false)
- **text** - a text field
    - `maxlength` - max character length of field
- **textarea** - a larger text box
    - `maxlength` - max character length of field
    - `rows` - the number of visible rows
- **toggleswitch** - similar to a checkbox but with a toggle switch view

**Additional Types**
- if one of the above types is not found then a input element will be created and the type attribute of the element will be set to the 'type' specified

Examples:

    let fields = [
        {type:'text',title:'Name', maxlength:100},
        {type:'number',title:'Age', attr:{min:0,step:1,max:130}},
        {type:'combobox', title:'Fruit', items:['Apple','Orange','Peach']}
        {type:'separator'},
        {type:'textarea',title:'address',wide:1}
    ]