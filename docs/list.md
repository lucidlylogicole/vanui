# Vanui.List / Vanui.Combobox
Easy way to add items to a list or combobox

## Demo
[List Demo](https://lucidlylogicole.gitlab.io/vanui/demos/list.html) - A vertical and horizontal splitter

## List API

**HTML**  

    <select id="li_fruit" size="6"></select>

**JS**

    let list = Vanui.List({
        element:document.getElementById('li_fruit'), 
        items:['Apple','Banana','Asparagus']
    }) 

**List(options)**

- `options`
    - `element` - the select element
    - `items` - list of items for the list
        - can be strings or object definition with values
        - `items:['Apple','Banana','Asparagus']`
        - `items:[{val:0,title:'Apple'},{val:1,title:'Banana'},{val:2,title:'Asparagus'}]`
- **.element** - the select element (created if options.element is undefined)
- **.val()** - get the value of the list
- **.setVal(val)** - set the value of the list
- **.clear()** - clear the list
- **.build(items)** - clear and build the list with a new set of items.  same definition as in options

## Combobox API
Very similar to the list

**HTML**  

    <select id="cb_fruit"></select>

**JS**

    let list = Vanui.Combobox({
        element:document.getElementById('li_fruit'), 
        items:['Apple','Bannana','Asparagus']
    }) 

**Combobox(options)**

- `options`
    - `element` - the select element
    - `items` - list of items for the list
        - can be strings or object definition with values
        - `items:['Apple','Banana','Asparagus']`
        - `items:[{val:0,title:'Apple'},{val:1,title:'Banana'},{val:2,title:'Asparagus'}]`
- **.element** - the select element (created if options.element is undefined)
- **.val()** - get the value of the list
- **.setVal(val)** - set the value of the list
- **.clear()** - clear the list
- **.build(items)** - clear and build the list with a new set of items.  same definition as in options