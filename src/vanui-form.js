try {Vanui} catch(e) {Vanui={}}

Vanui.Form = function(options) {

//---o:
let Form = {
    
    fields:[],
    
    build: function() {
        // Build the form
        Form.element = document.createElement('div')
        Form.element.classList.add('vanui-form')
        for (let i in options.fields) {
            Form.addField(options.fields[i])
        }
        // Add to parent
        if (options.parent) {
            Form.container = document.getElementById(options.parent)
            Form.container.appendChild(Form.element)
        }
        
    },
    
    addField: function(fieldD) {
        Form.fields.push(fieldD)
        let field_index = Form.fields.length-1
        
        // Row Elm
        let row_elm = document.createElement('div')
        row_elm.classList.add('form-row')
        row_elm.setAttribute('form-row-index',field_index)
        
        // Title Element
        let label_elm = document.createElement('label')
        if (fieldD.title) {
            label_elm.innerHTML = fieldD.title
        }
        row_elm.appendChild(label_elm)
        
        // Value alias
        if (fieldD.value) {fieldD.val = fieldD.value}
        
        // Input Element
        let input_elm
        if (fieldD.type == 'combobox' || fieldD.type == 'select' || fieldD.type == 'list') {
        //---Combobox and List
            input_elm = document.createElement('select')
            
            if (fieldD.type == 'list' || fieldD.size) {
                if (!fieldD.size) {fieldD.size=4}
                input_elm.setAttribute('size',fieldD.size)
                input_elm.classList.add('form-list')
            }
            if (fieldD.multiple) {
                input_elm.setAttribute('multiple','multiple')
            }
            
            // Add Items
            if (fieldD.items) {
                for (let item of fieldD.items) {
                    let opt_elm = document.createElement('option')
                    if (typeof(item) == 'object') {
                        opt_elm.value = item.val
                        opt_elm.text = item.title
                    } else {
                        opt_elm.value = item
                        opt_elm.text = item
                    }
                    input_elm.appendChild(opt_elm)
                }
            }
            
            // Add to Row
            row_elm.appendChild(input_elm)
            
        } else if (fieldD.type == 'button') {
        //---Button
            input_elm = document.createElement('button')
            input_elm.innerHTML = fieldD.text
            if (fieldD.val) {input_elm.innerText = fieldD.val}
            if (fieldD.click) {input_elm.addEventListener('click',fieldD.click)}
            
            // Add to Row
            row_elm.appendChild(input_elm)
            
        } else if (fieldD.type == 'textarea') {
        //---Text Area
            input_elm = document.createElement('textarea')
            
            let ta_rows = 4
            if (fieldD.rows) {ta_rows = fieldD.rows}
            input_elm.setAttribute('rows',ta_rows)
            
            // Add to Row
            row_elm.appendChild(input_elm)
        } else if (fieldD.type == 'separator') {
        //---Separator
            input_elm = document.createElement('div')
            
            if (fieldD.line) {
                label_elm.classList.add('form-separator-line')
            } else {
                label_elm.classList.add('form-separator')
            }
            
        } else {
        //---Input Element
            input_elm = document.createElement('input')
            if (fieldD.type) {
                input_elm.setAttribute('type',fieldD.type)
                
                // Checkbox
                if (fieldD.type == 'checkbox' || fieldD.type == 'toggleswitch') {
                    if (fieldD.type == 'toggleswitch') {
                        input_elm.classList.add('toggle-switch')
                        input_elm.setAttribute('type','checkbox')
                    }
                    
                    // Add container
                    let input_outer_elm = document.createElement('div')
                    input_outer_elm.classList.add('form-field-container')
                    input_outer_elm.appendChild(input_elm)
                    row_elm.appendChild(input_outer_elm)
                    
                } else {
                    // Add to Row
                    row_elm.appendChild(input_elm)
                }
            }

        }
        
        // Additional Element Attributes
        input_elm.classList.add('form-field')
        // ID
        if (fieldD.id) {
            input_elm.id = fieldD.id
            label_elm.setAttribute('for',fieldD.id)
        }
        
        // Tooltip
        if (fieldD.tooltip) {
            input_elm.title = fieldD.tooltip
            label_elm.title = fieldD.tooltip
        }

        // Set Max Length
        if (fieldD.maxlength) {
            input_elm.setAttribute('maxlength',fieldD.maxlength)
        }

        // Wide Setting
        if (fieldD.wide || options.wide) {
            row_elm.classList.add('wide')
        }
        
        // Add Other Attributes
        for (let attr in fieldD.attr) {
            input_elm.setAttribute(attr,fieldD.attr[attr])
        }
        
        // Add Events
        for (let evt in fieldD.events) {
            input_elm.addEventListener(evt,fieldD.events[evt])
        }
        
        // Store field elements
        fieldD.elm = {
            input:input_elm,
            label:label_elm,
            row:row_elm,
        }
        Form.element.appendChild(row_elm)
        
        // Set Default Value
        if (fieldD.val) {
            Form.setValue(field_index,fieldD.val)
        } else if (fieldD.default) {
            Form.setValue(field_index,fieldD.default)
        }
        
        return 
    },
    
    clearValues: function() {
        // Clear Values
        for (let fi in Form.fields) {
            let fieldD = Form.fields[fi]
            if (fieldD.default) {
                Form.setValue(fi,fieldD.default)
            } else {
                Form.setValue(fi,'')
            }
        }
    },
    
    setValue: function(field_index, val) {
        let fieldD = Form.fields[field_index]
        if (fieldD.type == 'select' || fieldD.type == 'list') {
            fieldD.elm.input.value = val
        } else if (fieldD.type == 'checkbox' || fieldD.type == 'toggleswitch') {
            fieldD.elm.input.checked = val
        } else if (fieldD.type == 'button') {
            fieldD.val = val
        } else {
            fieldD.elm.input.value = val
        }
    },
    
    setValues: function(valueD) {
        // Set Field Values
        
        let valD = valueD
        // Parse Array into dict
        if (Array.isArray(valueD)) {
            valD = {}
            for (let i in valueD) {
                valD[i] = valueD[i]
            }
        }
        
        for (let fi in Form.fields) {
            let fid = Form.fields[fi].id
            let val
            // Check if id or index in valD and set value
            if (fid in valD) {
                val = valD[fid]
            } else if (fi in valD) {
                val = valD[fi]
            }
            if (val) {form.setValue(fi,val)}
        }
    },
    
    getValue: function(field_index) {
        let fieldD = Form.fields[field_index]
        let val
        if (fieldD.type == 'select' || fieldD.type == 'list') {
            // check for multiple
            if (fieldD.elm.input.multiple) {
                val = []
                for (let opt_elm of fieldD.elm.input.children) {
                    if (opt_elm.selected) {
                        val.push(opt_elm.value)
                    }
                }
            } else {
                val = fieldD.elm.input.value
            }
        } else if (fieldD.type == 'checkbox' || fieldD.type == 'toggleswitch') {
            val = fieldD.elm.input.checked
        } else if (fieldD.type == 'button') {
            val = fieldD.val
        } else {
            val = fieldD.elm.input.value
        }
        return val
    },
    
    getValues: function(format) {
        // Get all field values
        if (format === undefined && Form.format !== undefined) {
            format = Form.format
        }
        let vals = []
        let valD = {}
        for (let fi in Form.fields) {
            let val = Form.getValue(fi)
            // if (!format) {
                // valD[fi] = val
            // } else if (format == 1) {
            if (format == 1) {
                if (Form.fields[fi].id) {
                    fid = Form.fields[fi].id
                    valD[fid] = val
                }
            } else {
                vals.push(val)
            }
        }
        if (format == 1){
            return valD
        } 
        return vals
    },
    
}

// Load Fields
// Form.fields = options.fields
Form.build()

// Set default format
if (options.format) {
    Form.format = options.format
}

// Set Values if provided
if (options.values) {
    Form.setValues(values)
}

return Form

}