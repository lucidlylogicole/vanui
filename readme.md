# Vanui - Vanilla JavaScript UI Widgets
Vanui is (will be) a basic set of widgets building on the standard html elements with a nice API and additional functionality.

Very basic styling is provided, leaving the custom styling up to the developer. 

## Widgets
- [combobox / list](list.md)
- dialog
- [form](docs/form.md)
- [splitter](docs/splitter.md)
- [table](docs/table.md)
- tabs
- tree
