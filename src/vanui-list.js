try {Vanui} catch(e) {Vanui={}}

Vanui.List = function(options) {

//---o:
let List = {
    element:options.element,
    items:[],
    val: function() {
        let val
        if (List.elm.input.multiple) {
            // Multiple values
            val = []
            for (let opt_elm of List.elm.input.children) {
                if (opt_elm.selected) {
                    val.push(opt_elm.value)
                }
            }
        } else {
            val = List.elm.input.value
        }
        return val
    },
    setVal: function(val) {
        List.elm.value = val
    },
    
    build: function(items) {
        List.clear()
        
        if (List.element === undefined) {
            List.element = document.createElement('select')
        }
        
        if (items.length > 0) {
            // Check for simple array or dict
            for (item of items) {
                if (typeof(item) == 'object') {
                    itemD = item
                } else {
                    itemD = {val:item,title:item}
                }
                
                let item_elm = document.createElement('option')
                item_elm.value = itemD.val
                item_elm.innerText = itemD.title
                List.element.appendChild(item_elm)
                List.items.push(itemD)
            }
            
        }
    },
    
    clear: function() {
        // Clear
        if (List.element) {
            while (List.element.firstChild) {List.element.removeChild(List.element.firstChild)}
        }
        List.items = []
    },
}

if (options.items) {
    List.build(options.items)
}

return List
}

Vanui.Combobox = function(options) {
    options.type='combobox'
    return Vanui.List(options)
}