try {Vanui} catch(e) {Vanui={}}

Vanui.Table = function(options) {

//---o:
let Table = {
    elm:{col_editors:{}}, // Store Elements
    selected:{},
    stack:[],
    sort: {
        enabled:0,
        column:undefined,
        descending:0,
    },
    pages:{ // pagination
        enabled:0,
        size:20,
        current:0,
    },
    editable:0,
    
    newDoc: function() {
        // Central Data
        //---d
        return {
            v:'0.3',
            counter:{row:100,col:100},
            data:{},
            columns:{},
            view:{rows:[],cols:[]},
        }
    },
    
    getDoc: function(get_json) {
        let doc = JSON.stringify(Table.d)
        if (get_json){return doc}
        return JSON.parse(doc)
    },

    setDoc: function(doc) {
        // Clear
        if (Table.elm.container) {
            while (Table.elm.container.firstChild) {Table.elm.container.removeChild(Table.elm.container.firstChild)}
        }
        
        if (typeof(doc) == 'string') {
            // json
            doc = JSON.parse(doc)
        }
        
        Table.d = Table.newDoc()
        for (let ky in doc) {
            if (ky != 'v') {  //ignore version
                Table.d[ky] = doc[ky]
            }
        }
        Table.build(options)
        Table.loadView()
    },

    build:function() {
        // Setup Main Containing Element
        Table.element = document.createElement('div')
        Table.element.style = "flex:1;display:flex;flex-direction:column;min-width:0;min-height:0;position:relative;"

        // Add to container if provided
        if (options.parent) {
            Table.elm.container = document.getElementById(options.parent)
            Table.elm.container.appendChild(Table.element)
        }
        
        // Table wrapping div
        Table.elm.table_div = document.createElement('div')
        Table.elm.table_div.style = "flex:1;overflow-y:auto;position:relative;overflow-x:auto"
        Table.element.appendChild(Table.elm.table_div)
        // Table Element
        Table.elm.table = document.createElement('table')
        if (options.tableId) {
            Table.elm.table.id = options.tableId
        }
        Table.elm.table.className = Table.elm.table.className+' vanui-table'
        Table.elm.table.setAttribute('tabindex',1)
        Table.elm.table_div.appendChild(Table.elm.table)
        // Setup Table Elements
        Table.elm.thead = document.createElement('thead')
        Table.elm.table.appendChild(Table.elm.thead)
        Table.elm.colheader = document.createElement('tr')
        Table.elm.thead.appendChild(Table.elm.colheader)
        Table.elm.cell_corner = document.createElement('th')
        Table.elm.cell_corner.className='cell-corner'
        Table.elm.colheader.appendChild(Table.elm.cell_corner)
        Table.elm.tbody = document.createElement('tbody')
        Table.elm.table.appendChild(Table.elm.tbody)
        
        // Add Pagination
        if (Table.pages.enabled) {
            Table.elm.pages = document.createElement('div')
            Table.element.appendChild(Table.elm.pages)
            Table.elm.pages.className = 'vanui-table-page-container'
            
            // First Page Button
            let pg_f = document.createElement('button')
            pg_f.innerHTML='&lsaquo;&lsaquo;'
            pg_f.title = 'first page'
            pg_f.className = 'vanui-table-btn'
            pg_f.addEventListener('click',function(){Table.loadView(0)})
            Table.elm.pages.appendChild(pg_f)
            // Last Page Button
            let pg_l = document.createElement('button')
            pg_l.innerHTML='&rsaquo;&rsaquo;'
            pg_l.title = 'last page'
            pg_l.className = 'vanui-table-btn'
            pg_l.addEventListener('click',function(){Table.loadView(Table.pageCount())})
            Table.elm.pages.appendChild(pg_l)
            
            // Input with Current Page
            Table.elm.page_input = document.createElement('input')
            Table.elm.page_input.setAttribute('type','number')
            Table.elm.page_input.className = 'vanui-table-i-page'
            Table.elm.page_input.addEventListener('change',function(){Table.loadView(parseInt(Table.elm.page_input.value)-1)})
            Table.elm.pages.appendChild(Table.elm.page_input)

            // Previous Button
            let pg_p = document.createElement('button')
            pg_p.innerHTML='&lsaquo;'
            pg_p.title = 'previous page'
            pg_p.className = 'vanui-table-btn'
            pg_p.addEventListener('click',Table.prevPage)
            Table.elm.pages.appendChild(pg_p)
            // Next Button
            let pg_n = document.createElement('button')
            pg_n.innerHTML='&rsaquo;'
            pg_n.title = 'next page'
            pg_n.className = 'vanui-table-btn'
            pg_n.addEventListener('click',Table.nextPage)
            Table.elm.pages.appendChild(pg_n)
            
            // On Wheel 
            Table.elm.table.addEventListener('wheel',Table.mouseWheel)
            
        }
        
        // Build Columns
        for (let colD of options.columns) {
            Table.buildColumn(colD)
        }

        // Event Listeners
        Table.elm.table.addEventListener('click',Table.click)
        Table.elm.table.addEventListener('dblclick',Table.dblclick)
        Table.elm.table.addEventListener('keydown',Table.keydown)
    },

    setData: function(data,delim) {
        if (delim === undefined) {delim='\t'}
        // Figure out Data Type
        let dtyp = typeof(data)
        if (dtyp == 'string') {
            //csv
            let csvdata = []
            for (let ln of data.split('\n')) {
                let rw = []
                for (let t of rw.split(delim)) {
                    rw.push(t)
                }
                csvdata.push(rw)
            }
            data = csvdata
        }
        
        // Parse Array
        Table.d.data = {}
        Table.d.view.rows = []
        for (let rw of data) {
            let rid = Table.d.counter.row++
            Table.d.view.rows.push(rid)
            Table.d.data[rid]={}
    
            // add cell data
            for (let c=0; c < Table.columnCount(); c++) {
                let cid = Table.d.view.cols[c]
                let col_typ = Table.d.columns[cid].type
                // Get Cell Value
                let val = null
                if (rw.length > c) {
                    val = rw[c]
                }
                if (val !== null) {
                    
                    if (col_typ == 'number') {
                        val = parseFloat(val)
                    } else {
                        val = String(val)
                    }
                    Table.d.data[rid][cid]={v:val}
                }
            }
        }
        
        // Reload View
        Table.loadView()
    },

    clear: function() {
        Table.d = Table.newDoc()
        Table.clearView(true)
        Table.loadView()
        
    },
    
    clearView: function(clear_header) {
        if (clear_header) {
            while (Table.elm.thead.firstChild) {Table.elm.thead.removeChild(Table.elm.thead.firstChild)}
        }
        
        // Clear table data
        while (Table.elm.tbody.firstChild) {Table.elm.tbody.removeChild(Table.elm.tbody.firstChild)}
    },

    //---
    loadView: function(page) {
        // Check if page is in range
        if (page !== undefined) {
            if (page < 0) {
                page = 0
            } else if (page > Table.pageCount()-1) {
                page = Table.pageCount()-1
            }
        }
        
        // Clear
        Table.clearView()
        
        // Find start and end rows
        let start = 0
        let end = Table.d.view.rows.length
        if (Table.pages.enabled) {
            if (page === undefined) {
                page = Table.pages.current
            } else {
                Table.pages.current = page
            }
            start = page*Table.pages.size
            end = start+Table.pages.size
            if (end > Table.d.view.rows.length) {
                end = Table.d.view.rows.length
            }
            Table.elm.page_input.value = Table.pages.current+1
        }
        
        // Build Rows
        for (let r=start; r<end; r++) {
            let rid = Table.d.view.rows[r]
            Table.buildRow(rid,r)
        }
        
    },
    
    //---
    //---Rows
    buildRow: function(rid,row_count,insert_row) {
        // Add the row with rid (row id) to the table
        let rw = []
        
        // Add Row Element
        let row_elm = document.createElement('tr')
        row_elm.setAttribute('data-rid',rid)
        let row_header_cell = document.createElement('th')
        row_header_cell.className='row-header-cell'
        row_header_cell.innerText=row_count+1
        row_elm.appendChild(row_header_cell)
        if (insert_row) {
            Table.elm.tbody.insertBefore(row_elm,Table.elm.tbody.childNodes[row_count])
        } else {
            Table.elm.tbody.appendChild(row_elm)
        }
    
        for (let c=0; c< Table.d.view.cols.length; c++) {
            let cid = Table.d.view.cols[c]
            
            // Check if column visible
            if (Table.d.columns[cid].visible){
                
                // Create Cell Element
                let cell_elm = document.createElement('td')
                
                // Get Cell Value
                let val = null
                if (Table.d.data[rid][cid] !== undefined) {
                    val = Table.d.data[rid][cid].v
    
                    // Cell Class and style if specified
                    if (Table.d.data[rid][cid].c) {
                        cell_elm.classList.add(Table.d.data[rid][cid].c)
                    }
                    if (Table.d.data[rid][cid].s) {
                        cell_elm.style = Table.d.data[rid][cid].s
                    }
                    
                }
                if (val === null) {val = ''}
                
                // Add Cell Elm
                if (Table.d.columns[cid].type == 'hyperlink') {
                    // Hyperlink
                    let a_elm = document.createElement('a')
                    a_elm.target="_blank"
                    if (Table.d.data[rid][cid] !==undefined && Table.d.data[rid][cid].h) {
                        a_elm.href = Table.d.data[rid][cid].h
                    }
                    cell_elm.appendChild(a_elm)
                } else {
                    // All other types
                    cell_elm.innerText = val
                }
                cell_elm.setAttribute('data-cid',cid)
                row_elm.appendChild(cell_elm)
            }
        }
    },
    
    insertRow: function(options) {
        if (options === undefined) {options = {}}
        
        // Add a new row, optionally before row id
        let rid = Table.d.counter.row++
        Table.d.data[rid]={}
        let row_count = 0
        if (options.before_rid === undefined) {
            // End
            Table.d.view.rows.push(rid)
            row_count = Table.d.view.rows.length-1
        } else if (options.before_rid == 0) {
            // Beginning
            options.before_rid = 0
            Table.d.view.rows.unshift(rid)
        } else {
            let cur_ind = Table.d.view.rows.indexOf(options.before_rid)
            row_count = cur_ind
            Table.d.view.rows.splice(cur_ind,0,rid)
        }
        
        let cur_col = Table.d.view.cols[0]
        if (Table.selected.col) {
            cur_col = Table.selected.col
        }
        
        if (Table.pages.enabled) {
            Table.selectCell(rid,cur_col)
        } else {
            
            // Add data if specified
            if (options.data !== undefined) {
                if (Array.isArray(options.data)) {
                    // Add Array
                    for (let i in options.data) {
                        let cid = tbl.d.view.cols[i]
                        Table.d.data[rid][cid]={v:options.data[i]}
                    }
                } else {
                    // Add Object
                    for (cid in options.data) {
                        // check for object
                        if (typeof(options.data[cid]) == 'object') {
                            Table.d.data[rid][cid]=options.data[cid]
                        } else {
                            Table.d.data[rid][cid]={v:options.data[cid]}
                        }
                    }
                }
            }
            
            // Add to table
            Table.buildRow(rid,row_count,true)
            Table.selectCell(rid,cur_col)
            
            // Recount if inserted before
            if (options.before_rid !== undefined && row_count < Table.d.view.rows.length-1) {
                Table.indexRows()
            }
        }
    },
    
    deleteRow: function(rid) {
        // Remove Data
        if (rid in Table.d.data) {
            delete Table.d.data[rid]
        }
        // Remove from view
        let cur_ind = Table.d.view.rows.indexOf(rid)
        Table.d.view.rows.splice(cur_ind,1)
        
        // Remove from Table
        if (Table.pages.enabled) {
            Table.loadView()
        } else {
            Table.elm.tbody.removeChild(Table.elm.tbody.querySelector(`tr[data-rid="${rid}"]`))
            Table.indexRows()
        }
        
        // Select next cell
        let cur_col = Table.selected.col
        let cur_row = 0
        if (cur_ind >= Table.d.view.rows.length) {
            cur_row = Table.d.view.rows[Table.d.view.rows.length-1]
        } else {
            cur_row = Table.d.view.rows[cur_ind]
        }
        Table.selectCell(cur_row,cur_col)
    },
    
    indexRows: function() {
        // Re-index the row labels
        let i = 0
        for (let row_elm of Table.elm.tbody.childNodes) {
            i++
            row_elm.childNodes[0].innerText=i
        }
    },
    
    rowCount:function(){return Table.d.view.rows.length},


    //---
    //---Columns
    buildColumn: function(colD) {
        // Add Column Info
        if (typeof(colD) == 'string') {
            colD = {title:colD}
        }
        
        // Add Column struct
        let cid = Table.d.counter.col++
        Table.d.columns[cid] = {
            title:colD.title,
            type:'text',
            // sort_toggle:1,
            editable:Table.editable,
            visible:1,
        }

        // Add Column Element
        let col_elm = document.createElement('th')
        col_elm.innerText = colD.title
        col_elm.setAttribute('data-cid',cid)

        // Add cid to list and element to header
        if (colD.before_cid === undefined) {
            // End
            Table.d.view.cols.push(cid)
            Table.elm.colheader.appendChild(col_elm)
        } else if (colD.before_cid == 0) {
            // Beginning
            Table.d.view.cols.unshift(cid)
            Table.elm.colheader.insertBefore(col_elm, Table.element.querySelector(`th[data-cid="${Table.d.view.cols[0]}"]`))
        } else {
            // Insert
            let cur_ind = Table.d.view.cols.indexOf(colD.before_cid)
            Table.d.view.cols.splice(cur_ind,0,cid)
            Table.elm.colheader.insertBefore(col_elm, Table.element.querySelector(`th[data-cid="${colD.before_cid}"]`))
        }
        
        // Add extra column attributes
        for (let col_key of ['editable','type','options','editCell','min','max','step','visible']) {
            if (col_key in colD) {
                Table.d.columns[cid][col_key] = colD[col_key]
            }
        }
        
        let col_typ = Table.d.columns[cid].type
        
        // Editable elements
        let inp_elm
        if (col_typ == 'select') {
            // Select
            inp_elm = document.createElement('select')
            // Add Items
            for (let opt of colD.items) {
                let opt_elm = document.createElement('option')
                opt_elm.value = opt
                opt_elm.text = opt
                inp_elm.appendChild(opt_elm)
            }

        } else {
            // Input
            inp_elm = document.createElement('input')
            if (col_typ == 'text' || col_typ == 'hyperlink') {
                inp_elm.setAttribute('type','text')
                inp_elm.style['text-align']='center'
            } else if (col_typ == 'number') {
                inp_elm.setAttribute('type','number')
                inp_elm.style['text-align']='center'
            } else if (col_typ == 'date') {
                inp_elm.setAttribute('type','date')
            }
        }
        inp_elm.style.position = 'absolute'
        inp_elm.style.display = 'none'
        // Table.elm.table.append(inp_elm)
        inp_elm.style['box-sizing']='border-box'
        inp_elm.change_ok = 1
        
        // Input Keypress Event
        inp_elm.addEventListener('keydown', function(event) {
            inp_elm.change_ok=0
            let kyc = event.key
            event.stopPropagation()
            if (kyc == "Enter") {
                // Enter
                inp_elm.change_ok=1
                event.preventDefault()
                Table.closeCellEdit()
            } else if (kyc == "ArrowDown" && event.altKey) {
                // Down + Alt Key enable cell edit
                inp_elm.change_ok=1
            } else if (kyc == "Escape") {
                // Esc
                Table.closeCellEdit(1)
            }
        },{bubbles:false})
        // Keyup Event
        inp_elm.addEventListener('keyup', function(event) {
            inp_elm.change_ok=1
        },{bubbles:false})

        // On change event
        inp_elm.addEventListener('change', function(event) {
            if (inp_elm.change_ok) {
                Table.closeCellEdit()
            }
        })

        Table.elm.col_editors[cid]=inp_elm
        
        // Check Visibility
        if (!Table.d.columns[cid].visible) {
            Table.setColumnVisibility(cid,0)
        }
        
        return cid
        
    },

    addColumn: function(options) {
        if (options === undefined) {options = {}}
        else if (typeof(options) == 'string') {
            options = {title:options}
        }

        let cid = Table.buildColumn(options)
        
        // Reload view
        Table.loadView()

    },
    
    deleteColumn: function(cid) {

        // Delete column header
        let col_th = Table.element.querySelector(`th[data-cid="${cid}"]`)
        col_th.parentElement.removeChild(col_th)
        
        // Delete all Cells
        let cell_elms = Table.element.querySelectorAll(`td[data-cid="${cid}"]`)
        for (let elm of cell_elms) {
            elm.parentElement.removeChild(elm)
        }
        
        // Delete from column settings
        Table.d.view.cols.splice(Table.d.view.cols.indexOf(cid),1)
        
        // Delete from data
        for (let rid in Table.d.data) {
            let rwD = Table.d.data[rid]
            if (cid in rwD) { delete rwD[cid] }
        }
        
    },
    
    setColumnVisible: function(cid,visible) {
        
        Table.d.columns[cid].visible = visible
        
        let cell_elms = Table.element.querySelectorAll(`[data-cid="${cid}"]`)
        for (let elm of cell_elms) {
            if (visible) {
                elm.style.display='table-cell'
            } else {
                elm.style.display='none'
            }
        }

    },
    
    columnCount:function() {return Table.d.view.cols.length},
    
    isEditable: function(cid) {
        // Is Table or Column Editable
        let editable = Table.editable
        if (editable && cid!==undefined) {
            editable = Table.d.columns[cid].editable
        }
        return editable
    },
    
    getVisibleColumns: function() {
        let vis_cols = []
        for (cid of Table.d.view.cols) {
            if (Table.d.columns[cid].visible) {
                vis_cols.push(cid)
            }
        }
        return vis_cols
    },
    
    //---
    //---Events
    click: function(event) {
        let elm = event.target
        // Remove Current Selection
        if (elm.tagName == 'TD') {
            // Table.selectCellElement(elm)
            let cid = parseInt(elm.getAttribute('data-cid'))
            let rid = parseInt(elm.parentElement.getAttribute('data-rid'))
            Table.selectCell(rid,cid)
        }
    },
    
    dblclick: function(event) {
        let elm = event.target
        if (elm.tagName == 'TD') {
            // Cell
            Table.editCell()
            // Double Click event
            let cid = parseInt(elm.getAttribute('data-cid'))
            let rid = parseInt(elm.parentElement.getAttribute('data-rid'))
            var evt = new CustomEvent('cellDblClick',{detail:{rid:rid,cid:cid}})
            Table.elm.table.dispatchEvent(evt)
        } else if (elm.tagName == 'TH' && elm.getAttribute('data-cid')) {
            // Column Header
            if (Table.sort.enabled) {
                Table.sortColumn(elm.getAttribute('data-cid'))
            }
        }
    },
    
    keydown: function(event) {
        let kyc = event.key
        // console.log(kyc,event.code,event.key)
        let ok = 1
        if (kyc=="End" || (event.ctrlKey && kyc == "ArrowDown")) {
            // Ctrl + Down (move to end)
            Table.selectCell(Table.d.view.rows.slice(-1)[0],Table.selected.col)
        } else if (kyc=="Home" || (event.ctrlKey && kyc == "ArrowUp")) {
            // Ctrl + Up (move to start)
            Table.selectCell(Table.d.view.rows[0],Table.selected.col)
        } else if (kyc == "ArrowUp") {
            if (event.altKey && Table.pages.enabled) {
                Table.prevPage()
            } else {
                // Up
                Table.moveCellSelection(-1,0)
            }
        } else if (kyc == "ArrowDown") {
            if (event.altKey && Table.pages.enabled) {
                Table.nextPage()
            } else {
                // Down
                Table.moveCellSelection(1,0)
            }
        } else if (kyc == "ArrowLeft") {
            // Left
            Table.moveCellSelection(0,-1)
        } else if (kyc == "ArrowRight") {
            // Right
            Table.moveCellSelection(0,1)
        } else if (kyc == "PageUp") {
            // Page Up
            if (Table.pages.enabled) {
                Table.prevPage()
            } else {
                ok = 0
            }
        } else if (kyc == "PageDown") {
            // Page Down
            if (Table.pages.enabled) {
                Table.nextPage()
            } else {
                ok = 0
            }
        } else if (kyc == "Enter") {
            // Enter
            Table.editCell()
        } else if (event.ctrlKey && kyc == "c") {
            // Ctrl+C (Copy)
            Table.copyVal=Table.d.data[Table.selected.row][Table.selected.col].v
        } else if (event.ctrlKey && kyc == "v") {
            // Ctrl+V (Paste)
            if (Table.copyVal !==undefined && Table.isEditable(Table.selected.col)) {
                Table.setCell(Table.selected.row,Table.selected.col,Table.copyVal)
            }
        } else if (event.ctrlKey && kyc == "x") {
            // Ctrl+X (Cut)
            if (Table.isEditable(Table.selected.col)) {
                Table.copyVal=Table.d.data[Table.selected.row][Table.selected.col].v
                Table.setCell(Table.selected.row,Table.selected.col,'')
            }
        } else if (kyc == "Delete") {
            // Delete
            if (Table.isEditable()) {
                if (event.altKey || event.ctrlKey) {
                    // Delete Row
                    Table.deleteRow(Table.selected.row)
                } else if (Table.isEditable(Table.selected.col)) {
                    // Delete cell
                    Table.setCell(Table.selected.row,Table.selected.col,'')
                }
            }
        } else if (event.altKey && kyc == 'n'){
            if (Table.isEditable()) {
                // Alt+N - New Row
                let before_rid = undefined
                if (Table.selected.row !== undefined) {
                    before_rid = Table.d.view.rows[Table.d.view.rows.indexOf(Table.selected.row)+1]
                }
                Table.insertRow({before_rid:before_rid})
            }
        } else if (event.ctrlKey && kyc == 'k') {
            // Edit Hyperlink
            Table.editCellHyperlink(Table.selected.row,Table.selected.col)
        } else if (kyc == 'Tab'){
            ok = 0
        } else if (!event.ctrlKey && !event.altKey) {
            if (kyc.length < 2) {
                Table.editCell(Table.selected.row,Table.selected.col,kyc)
            }
        } else {
            ok = 1
        }
        
        if (ok) {event.preventDefault()}
    },

    mouseWheel: function(event) {
        if (!event.shiftKey) {
            event.stopPropagation()
            if (event.deltaY > 0) {
                Table.nextPage()
            } else {
                Table.prevPage()
            }
        }
    },

    //---
    //---Cell
    cellElement: function(rid,cid) {
        return Table.elm.table.querySelector(`tr[data-rid="${rid}"] td[data-cid="${cid}"]`)
    },
    
    cellVal: function(rid,cid) {
        let val
        if (rid in Table.d.data && cid in Table.d.data[rid]) {
            val = Table.d.data[rid][cid].v
        }
        return val
    },
    
    setCell: function(rid,cid,val) {
        // let col_typ = Table.d.columns[Table.selected.col].type
        let col_typ = Table.d.columns[cid].type
        
        // Add column to row if undefined
        if (Table.d.data[rid][cid]==undefined) {
            Table.d.data[rid][cid] = {}
        }
        
        if (val == undefined || val == '' || val == null) {
            // Blank Value
            Table.cellElement(rid,cid).innerHTML=''
            Table.d.data[rid][cid].v=null
        } else {
            if (col_typ == 'number') {
                val = parseFloat(val)
            }
            Table.d.data[rid][cid].v=val
            if (col_typ == 'hyperlink') {
                Table.cellElement(rid,cid).children[0].innerText=val
            } else {
                Table.cellElement(rid,cid).innerText=val
            }
        }
        
        // Change Event
        var evt = new CustomEvent('cellChanged',{detail:{rid:rid,cid:cid,val:val}})
        Table.elm.table.dispatchEvent(evt)
        
    },

    selectCell: function(rid,cid) {
        // Check pagination
        if (Table.pages.enabled) {
            let rc = Table.d.view.rows.indexOf(rid)
            let pg = Math.floor(rc/Table.pages.size)
            if (pg != Table.pages.current) {
                Table.loadView(pg)
            }
        }
        
        let cell_elm = Table.cellElement(rid,cid)
        Table.selectCellElement(cell_elm)
        
        // Change Event
        var evt = new CustomEvent('cellSelected',{detail:{rid:rid,cid:cid}})
        Table.elm.table.dispatchEvent(evt)
        
    },
    
    getCellIds: function(row,col) {
        // Get rid and cid of current row and column indexes
        return [Table.d.view.rows[row],Table.d.view.cols[col]]
    },
    
    selectCellElement: function(elm) {
        // Select Cell
        Table.elm.table.querySelectorAll('.selected').forEach(sel_elm=>{
            sel_elm.classList.remove('selected')
        })
        elm.classList.add('selected')
        
        // Select Row
        Table.elm.table.querySelectorAll('tr.selected').forEach(sel_elm=>{
            sel_elm.classList.remove('selected')
        })
        elm.closest('tr').classList.add('selected')
        
        Table.selected.row = parseInt(elm.parentElement.getAttribute('data-rid'))
        Table.selected.col = parseInt(elm.getAttribute('data-cid'))
        Table.closeCellEdit(1)
        
        // Scroll
        elm.scrollIntoView({block: "nearest"})
        
        // Fix For Sticky Headers
        // Check if cell above sticky header and correct
        let elm_rect = elm.getBoundingClientRect()
        let corner_rect = Table.elm.cell_corner.getBoundingClientRect()
        if (elm_rect.top < corner_rect.bottom) {
            Table.elm.table.parentElement.scrollBy(0,elm_rect.top - corner_rect.bottom)
        }
        // Check if cell behind row sticky header and correct
        if (elm_rect.left < corner_rect.right) {
            Table.elm.table.parentElement.scrollBy(elm_rect.left - corner_rect.right,0)
        }
    },
    
    moveCellSelection: function(row_inc,col_inc) {
        let rw = -1
        let col = -1
        
        // Check Selected Row
        if (Table.selected.row !== undefined) {
            rw = Table.d.view.rows.indexOf(Table.selected.row)
        }
        // Increment row and check in range
        rw = rw+row_inc
        if (rw >= Table.rowCount()) {
            rw = Table.rowCount()-1
        } else if (rw < 0) {
            rw = 0
        }
        
        // Columns
        let vis_cols = Table.getVisibleColumns()
        
        // Check Selected Column
        if (Table.selected.col !== undefined) {
            col = vis_cols.indexOf(Table.selected.col)
        }
        // Increment column
        col = col + col_inc
        
        // Check Range
        if (col >= vis_cols.length) {
            col = vis_cols.length-1
        } else if (col < 0) {
            col = 0
        }
        
        // Select Cell
        if (rw !=-1 && col!=-1) {
            Table.selectCell(Table.d.view.rows[rw],vis_cols[col])
        }
        
    },
    
    //---Cell Edit
    editCell: function(rid,cid,val) {
        if (rid === undefined) {
            if (Table.selected.row !== undefined) {
                rid = Table.selected.row
            }
        }
        if (cid === undefined) {
            if (Table.selected.col !== undefined) {
                cid = Table.selected.col
            }
        }
        
        if (!Table.isEditable(cid)) {
            return
        }
        
        // Create
        let cell_elm = Table.cellElement(rid,cid)
        if (cell_elm) {
            
            if (val===undefined) {
                val = Table.cellVal(rid,cid)
            }
            
            // If custom column column edit function
            if (Table.d.columns[cid].type == 'custom') {
                Table.d.columns[cid].editCell(rid,cid,val)
                return
            }
            
            // Get Column Input Field
            let inp_elm = Table.elm.col_editors[cid]
            Table.current_edit_element = inp_elm
            Table.elm.table.append(inp_elm)
            inp_elm.style.left = cell_elm.offsetLeft+'px'
            inp_elm.style.top = cell_elm.offsetTop+'px'
            inp_elm.style.width = cell_elm.offsetWidth+'px'
            inp_elm.style.height = cell_elm.offsetHeight+'px'
            inp_elm.style.display='block'
            inp_elm.focus()
            if (val === undefined) {
                // inp_elm.value = cell_elm.innerText
                inp_elm.value = ''
            } else {
                inp_elm.value = val
            }
        }
    },
    
    closeCellEdit: function(esc) {
        if (Table.current_edit_element !== undefined) {
            Table.current_edit_element.style.display='none'
            if (!esc) {
                Table.setCell(Table.selected.row,Table.selected.col,Table.current_edit_element.value)
            }
            Table.current_edit_element = undefined
            Table.elm.table.focus()
        }
    },
    
    //---Hyperlink
    setCellHyperlink: function(rid,cid,url) {

        if (Table.d.data[rid][cid]===undefined) {
            Table.d.data[rid][cid] = {}
        }
        Table.d.data[rid][cid].u = url
        if (url == '') {
            Table.cellElement(rid,cid).children[0].removeAttribute('href')
        } else {
            Table.cellElement(rid,cid).children[0].href=url
        }
    
        // Change Event
        var evt = new CustomEvent('cellChanged',{detail:{rid:rid,cid:cid,url:url}})
        Table.elm.table.dispatchEvent(evt)
        
    },
    
    editCellHyperlink: function(rid,cid,url) {
        if (url === undefined) {
            url = ''
            if (Table.d.data[rid][cid] && Table.d.data[rid][cid].u) {
                url = Table.d.data[rid][cid].u
            }
        }
        let new_url = prompt('Edit the Link/URL',url)
        if (new_url !== null) {
            tbl.setCellHyperlink(rid,cid,new_url)
        }
    },
    
    //---
    //---Pagination (pages)
    pageCount:function(){return Math.ceil(Table.d.view.rows.length/Table.pages.size)},
    
    nextPage: function() {
        Table.loadView(Table.pages.current+1)
    },

    prevPage: function() {
        Table.loadView(Table.pages.current-1)
    },
    
    gotoPage: function(page) {
        loadView(page)
    },
    
    //---
    //---Sorting
    sortColumn: function(cid, descending, sort_function) {
        
        // Check for descending
        if (descending === undefined) {
            if (Table.sort.column == cid) {
                descending = !Table.sort.descending
            } else {
                descending = 0
            }
        }
        Table.sort.column = cid
        Table.sort.descending = descending
        
        // Set up a row array to sort
        let rows_array = []
        for(let r=0; r<Table.d.view.rows.length; r++) {
            let rid = Table.d.view.rows[r]
            let cv = null
            if (Table.d.data[rid][cid]!==undefined){cv = Table.d.data[rid][cid].v}
            rows_array[r]={old_index:rid, val:cv}
            // If sort by number
            if (Table.d.columns[cid].type == 'number') {
                let v = parseFloat(rows_array[r].val)
                if (!isNaN(v)){rows_array[r].val = v}
            }
        }
        
        // Sort the Array
        if (sort_function !== undefined) {
            // User Defined Sort Function
            rows_array.sort(sort_function)
        } else {
            rows_array.sort((a,b)=>{
                return(a.val == b.val ? 0 : (a.val > b.val ? 1 : -1))
            })
        }
        
        // Update Column indicator class
        let elm_classname = 'sort-asc'
        // Check Descending
        if (descending) {
            rows_array.reverse()
            elm_classname = 'sort-desc'
        }
        
        // Re-append everything to the table
        let new_row_order = []
        for(let r=0; r<rows_array.length; r++) {
            let rid = rows_array[r].old_index
            new_row_order.push(rid)
        }
        Table.d.view.rows = new_row_order
        
        // Set Sort Icon
        Table.elm.table.querySelectorAll('th.sort').forEach(sel_elm=>{
            sel_elm.classList.remove('sort')
            sel_elm.classList.remove('sort-asc')
            sel_elm.classList.remove('sort-desc')
        })
        let th_elm = Table.elm.thead.querySelector(`th[data-cid="${cid}"]`)
        th_elm.classList.add('sort')
        th_elm.classList.add(elm_classname)

        // Reload the view
        Table.loadView()

    },
    
    //---Exports
    getData: function(options) {
        // Generate Nice Data Format
        
        if (options === undefined) { options = {}}
        let data = {columns:[],data:[]}
        
        // Get Columns
        for (let cid of Table.d.view.cols) {
            data.columns.push(Table.d.columns[cid].title)
        }
        
        // Get Data
        for (let rid of Table.d.view.rows) {
            let rw = []
            
            for (let cid of Table.d.view.cols) {
                let v = undefined
                if (cid in Table.d.data[rid]) {
                    v = Table.d.data[rid][cid].v
                }
                if (options.blank && v===undefined) {
                    v = ''
                }
                rw.push(v)
            }
            data.data.push(rw)
        }
        
        return data
    },
    
    getText: function(delim) {
        // Delimited Text
        if (delim === undefined) {delim = '\t'}
        let data = Table.getData({blank:1})
        let txt = data.columns.join(delim)
        for (let rw of data.data) {
            txt += '\n'+rw.join(delim)
        }
        return txt
    },
    
    
} // end Table

//---
//---Main Loading
Table.d = Table.newDoc()
if (options.editable) {Table.editable=1}
if (options.sort) {Table.sort.enabled=1}
if (options.pages !== undefined) {
    for (let pky in Table.pages) {
        if (pky in options.pages) {
            Table.pages[pky] = options.pages[pky]
        }
    }
}

if (options.doc) {
    // Load the document
    Table.setDoc(options.doc)
} else {
    // Load other table format
    
    // Build the Table
    Table.build(options)
    
    if (options.data) {
        Table.setData(options.data)
    }
    
}


return Table

}